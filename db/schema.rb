# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160818163046) do

  create_table "banners", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "lead",       limit: 65535
    t.string   "url",        limit: 255
    t.integer  "active",     limit: 4
    t.datetime "active_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "banners", ["id"], name: "id", unique: true, using: :btree

  create_table "cakes", force: :cascade do |t|
    t.integer  "category_id",       limit: 4,     null: false
    t.string   "name",              limit: 255
    t.text     "content",           limit: 65535
    t.text     "recipe",            limit: 65535
    t.text     "nutrition_content", limit: 65535
    t.string   "thumb",             limit: 255
    t.string   "banner",            limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "cakes", ["id"], name: "id", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "excerpt",            limit: 65535
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 45
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "categories", ["id"], name: "id", unique: true, using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "state_id",   limit: 8
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "cities", ["id"], name: "id", unique: true, using: :btree
  add_index "cities", ["state_id"], name: "cities_state_id_index", using: :btree

  create_table "clippings", force: :cascade do |t|
    t.string   "title",        limit: 255
    t.text     "content",      limit: 65535
    t.string   "avatar",       limit: 255
    t.string   "font",         limit: 255
    t.datetime "published_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "newsletters", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.integer  "active",     limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "notices", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.text     "excerpt",    limit: 65535
    t.text     "content",    limit: 65535
    t.string   "thumb",      limit: 255
    t.string   "slug",       limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "notices", ["id"], name: "id", unique: true, using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "uf",         limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "states", ["id"], name: "id", unique: true, using: :btree

  create_table "stores", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "tel",        limit: 255
    t.string   "cel",        limit: 255
    t.string   "waths",      limit: 255
    t.text     "hour",       limit: 65535
    t.string   "address",    limit: 255
    t.string   "district",   limit: 255
    t.string   "lat",        limit: 255
    t.string   "lng",        limit: 255
    t.integer  "city_id",    limit: 8
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "stores", ["city_id"], name: "stores_city_id_index", using: :btree
  add_index "stores", ["id"], name: "id", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.string   "role",            limit: 255
    t.integer  "activated",       limit: 4
    t.datetime "activated_at"
    t.string   "password_token",  limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "users", ["id"], name: "id", unique: true, using: :btree

  add_foreign_key "cities", "states", name: "cities_state_id_fkey"
  add_foreign_key "stores", "cities", name: "stores_city_id_fkey"
end
