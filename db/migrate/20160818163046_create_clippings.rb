class CreateClippings < ActiveRecord::Migration
  def change
    create_table :clippings do |t|
      t.string :title
      t.text :content
      t.string :avatar
      t.string :font
      t.datetime :published_at

      t.timestamps null: false
    end
  end
end
