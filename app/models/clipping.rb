class Clipping < ActiveRecord::Base
  # Create slugs
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  #You don't necessarily need this bit, but I have it in there anyways
  def should_generate_new_friendly_id?
   slug.nil? || title_changed?
  end
  # Validating Images
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates :avatar, attachment_presence: true
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  validates :title, :content, presence: true
end
