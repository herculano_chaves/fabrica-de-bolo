class User < ActiveRecord::Base
  has_secure_password

  validates :name, :email, :password_confirmation, presence: true
  validates :email,  uniqueness: true
  validates :password, confirmation: true

  def is_admin?
    self.role == "admin"
  end

end
