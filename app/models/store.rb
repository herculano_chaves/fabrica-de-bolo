class Store < ActiveRecord::Base
  geocoded_by :address, :latitude  => :lat, :longitude => :lng
  after_validation :geocode

  def address
    [:address, :num, :district].compact.join(', ')
  end
end
