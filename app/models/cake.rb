class Cake < ActiveRecord::Base
  belongs_to :category

  has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates :photo, attachment_presence: true
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/

  validates_presence_of :name, :content, :nutrition_content, :recipe, :category_id, :message => "é um campo obrigatório"
end
