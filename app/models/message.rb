class Message < ActiveRecord::Base
  validates_presence_of :name, :email, :subject, :msg, :message => "é um campo obrigatório"
  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/, :message => "esta com formato inválido"

end
