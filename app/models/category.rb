class Category < ActiveRecord::Base
  has_many :cakes
  # Create slugs
  extend FriendlyId
  friendly_id :name, use: [:slugged, :finders]

  #You don't necessarily need this bit, but I have it in there anyways
  def should_generate_new_friendly_id?
   slug.nil? || name_changed?
  end
  # Validating Images
  has_attached_file :photo, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates :photo, attachment_presence: true
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/

  validates :name, :excerpt, presence: true
end
