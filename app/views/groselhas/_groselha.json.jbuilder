json.extract! groselha, :id, :name, :tipo, :email, :created_at, :updated_at
json.url groselha_url(groselha, format: :json)