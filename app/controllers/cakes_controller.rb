class CakesController < ApplicationController
  layout "admin", :only => [:index, :new, :create, :edit, :update]
  before_action :require_user, :only => [:index]
  before_action :set_cake, :only => [:edit, :update, :destroy]
  def index
    @cakes = Cake.all
  end
  def new
    @cake = Cake.new
  end
  def create
    @cake = Cake.new(cake_params)
      puts cake_params.inspect
    if @cake.save
      flash[:success] = "Adicionado com sucesso"
      redirect_to cakes_url
    else
      render 'new'
    end
  end
  def edit
  end
  def update
    @cake.update(cake_params)
    if @cake.save
      flash[:success] = "Bolo atualizado com sucesso"
      redirect_to edit_cake_url
    else
      render :edit
    end
  end
  def destroy
    @cake.delete
    flash[:success] = "Bolo deletado"
    redirect_to categories_url
  end

  private
  def set_cake
    @cake = Cake.find(params[:id])
  end
  def cake_params
    params.require(:cake).permit(:name, :recipe, :content, :nutrition_content, :category_id, :photo)
  end
end
