class PageController < ApplicationController
	def index

	end
	def novidades
	  #code
	end
	def imprensa
		#code
	end
	def onde_encontrar
		#code
	end
	def contato
		@message = Message.new
	end
	def post_contato
		@message = Message.new(message_params)
		if @message.valid?
			  MessageMailer.contact_message(params[:message]).deliver_later
				flash[:success] = "Mensagem enviada com sucesso"
				redirect_to contato_url
		else
			render :contato
		end
	end
	private
	def message_params
		params.require(:message).permit(:name, :subject, :email,:msg)
	end
end
