class SessionsController < ApplicationController

  layout "login", :only => [:new]
  before_action :already_logged, :only => [:new]

  def new

  end
  def create
  	@user = User.find_by_email(params[:session][:email])
  	if @user && @user.authenticate(params[:session][:password]) && @user.is_admin?
    	session[:user_id] = @user.id
    	redirect_to '/admin/cakes'
  	else
      flash[:error] = "Usuário não encontrado"
    	redirect_to '/admin'
  	end
  end
  def destroy
  	session[:user_id] = nil
    redirect_to '/admin'
  end
end
