class CategoriesController < ApplicationController
  layout "admin", :except => [:show]
  before_action :set_category, :only => [:edit, :update, :destroy]
  def index
    @categories = Category.order(id: :desc).page(params[:page]).per(2)
  end
  def show
    #code
  end
  def new
    @category = Category.new
  end
  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:success] = "Categoria criada com sucesso"
      redirect_to categories_url
    else
      render :new
    end
  end
  def edit
    # Edição de categoria
  end
  def update
    @category.update(category_params)
    if @category.save
      flash[:success] = "Categoria atualizada com sucesso"
      redirect_to edit_category_url(@category)
    else
      render :edit
    end
  end
  def destroy
    @category.delete
    flash[:success] = "Categoria deletada"
    redirect_to categories_url
  end

  private
  def set_category
    @category = Category.find(params[:id])
  end
  def category_params
    params.require(:category).permit(:name,:excerpt,:photo)
  end
end
