class NewslettersController < ApplicationController
  def create
    @newsletter = Newsletter.new(newsletter_params)
    respond_to do |format|
      if @newsletter.save
        #format.html { redirect_to @newsletter, notice: "Save process completed!" }
        format.json { render :json => @newsletter }
      else 
        format.json {render :json => @newsletter.errors }
      end
    end
  end
  private
  def newsletter_params
    params.require(:newsletter).permit(:name, :email,:active)
  end
end
