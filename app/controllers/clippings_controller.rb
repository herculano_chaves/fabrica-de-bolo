class ClippingsController < ApplicationController
  layout "admin", :except => [:show]
  before_action :set_category, :only => [:edit, :update, :destroy]
  def index
    @clippings = Clipping.order(id: :desc).page(params[:page]).per(2)
  end
  def show
    #code
  end
  def new
    @clipping = Clipping.new
  end
  def create
    #d = params[:published_at].split("/")



  #  @clipping = Clipping.new(clipping_params)
  #  if @clipping.save
  #    flash[:success] = "Categoria criada com sucesso"
  #    redirect_to categories_url
  #  else
  #    render :new
  #  end

  end
  def edit
    # Edição de categoria
  end
  def update
    @clipping.update(clipping_params)
    if @clipping.save
      flash[:success] = "Categoria atualizada com sucesso"
      redirect_to edit_clipping_url(@clipping)
    else
      render :edit
    end
  end
  def destroy
    @clipping.delete
    flash[:success] = "Categoria deletada"
    redirect_to categories_url
  end

  private
  def set_clipping
    @clipping = Clipping.find(params[:id])
  end
  def clipping_params
    params.require(:clipping).permit(:title, :content, :font, :avatar, :published_at)
  end
end
