class ApplicationMailer < ActionMailer::Base
  default from: "contato@fabricadebolo.com.br"
  layout 'mailer'
end
