class MessageMailer < ApplicationMailer
  def contact_message(message)
    @message = message
    @time = Time.new
    mail(to: 'herculano@dizain.com.br', subject: 'Mensagem do formulário de contato Fabrica de Bolo')
  end
end
