$(document).ready(function(){

	$(window).on('load resize', function(){
		$('.navbar-top-text').css({'left':$('.navbar-right').offset().left-10});
		$('#map').parents('.row').find('>[class*="col-"]').height($(window).height() - $('header nav').height() - $('.search').height() - 26);
		$('#banner .carousel-inner>.item').css({'height':$(window).height()});
	});

	$('.news_create').click(function(){
		$.ajax({
				type: 'post',
				data: { newsletter: { name: $('#name_news').val(), email: $('#email_news').val(), active: '1' } },
				url: '/newsletters/create',
				dataType: "json"
		}).done(function(data){
			$('#name_news').val('');
			$('#email_news').val('');
			if(data.id){
				$('#res').html(data.name+' inscrito com sucesso!');
			}else{
				var r = "";
				$.each(data, function(i,v){
					r+=i+": "+v+"<br/>";

				});
				$('#res').html(r);
			}

			$('#res').fadeIn().delay(2000).fadeOut();


		});
	});

	$("#radio").slider();

	$('.col-list-map ul').on('load scroll', function(){
		if($(this).scrollTop() > 0){
			$(this).parent('.list-shadow').find('.list-shadow.top').addClass('active');
		}else{
			$(this).parent('.list-shadow').find('.list-shadow.top').removeClass('active');
		}
	});

	//scrollReveal
    window.scrollReveal = new scrollReveal({ reset: false });

    $("#banner").on('slide.bs.carousel', function (e) {
        $(".active", e.target).find('.carousel-text h2').animate({'opacity':'0'}, function(){
        	$(this).animate({'margin-top':'10px'});
        });

        $(".active", e.target).find('.carousel-text p').animate({'opacity':'0'}, function(){
        	$(this).animate({'margin-top':'10px'});
        });
    });

    $("#banner").on('slid.bs.carousel', function (e) {
    	$(".active", e.target).find('.carousel-text h2').animate({'margin-top':'0', 'opacity':'1'});
    	$(".active", e.target).find('.carousel-text p').animate({'margin-top':'0', 'opacity':'1'});
    });



});
