require 'test_helper'

class GroselhasControllerTest < ActionController::TestCase
  setup do
    @groselha = groselhas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:groselhas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create groselha" do
    assert_difference('Groselha.count') do
      post :create, groselha: { email: @groselha.email, name: @groselha.name, tipo: @groselha.tipo }
    end

    assert_redirected_to groselha_path(assigns(:groselha))
  end

  test "should show groselha" do
    get :show, id: @groselha
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @groselha
    assert_response :success
  end

  test "should update groselha" do
    patch :update, id: @groselha, groselha: { email: @groselha.email, name: @groselha.name, tipo: @groselha.tipo }
    assert_redirected_to groselha_path(assigns(:groselha))
  end

  test "should destroy groselha" do
    assert_difference('Groselha.count', -1) do
      delete :destroy, id: @groselha
    end

    assert_redirected_to groselhas_path
  end
end
